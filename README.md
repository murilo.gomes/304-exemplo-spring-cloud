Exercício de pagamentos
O objetivo desse exercicio é práticar os conceitos vistos em aula sobre FeignClient e Eureka

Regras

Deve haver um servidor dedicado a Eureka
Tanto o cartão quanto o pagamentos devem ser subidos em instancias diferentes de Spring Cloud
A instancia de cartão e a de pagamentos não podem se comunicar informando o URL direto da aplicação



Requisições de cartao:

GET /cartao/{numero}
Exibe um cartão do sistema
Response 200
{
    "id": 1,
    "numero": "012730871"
}

POST /cartao
Cria um cartao no sistema.
Request Body
{
    "numero": "012730871"
}
Response 200
{
    "id": 1,
    "numero": "012730871"
}

Requisições de pagamento:

GET /pagamentos/{id_cartao}
Exibe os pagamentos de um cartao (extrato)
Response 200
[
    {
        "id": 1,
        "cartao_id": 1,
        "descricao": "pizza",
        "valor": 20.5 
    },
    {
        "id": 2,
        "cartao_id": 1,
        "descricao": "cerveja",
        "valor": 10.5 
    }
]

POST /pagamento
Cria um pagamento no sistema.
Request Body
{
    "cartao_id": 1,
    "descricao": "cerveja",
    "valor": 10.5 
}
Response 200
{
    "id": 1,
    "cartao_id": 1,
    "descricao": "cerveja",
    "valor": 10.5 
}